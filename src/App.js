import React from 'react';
import { Switch, BrowserRouter,  Route } from 'react-router-dom';
import './App.css';
import Home from './Home.js';
import History from './History.js';
import Navbar from './Navbar.js';

function App() {
  return (
    <>
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/our-history" component={History}/>
       
        
          
        
      </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
